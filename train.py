from chatterbot import ChatBot
from chatterbot.trainers import ChatterBotCorpusTrainer

chat = ChatBot(                                               # se declara la entidad del chat, nueva instancia del bot
    'chatterbot',                                             # nombre del bot
    storage_adapter='chatterbot.storage.SQLStorageAdapter',   # adaptador para el almacenamiento en SQLite
    #database='./data/database.sqlite',                       # ubicacion del archivo SQLite donde se guardan los datos
    trainer='chatterbot.trainers.ListTrainer',                # adaptador logico para el entrenamiento
)

trainer = ChatterBotCorpusTrainer(chat)

trainer.train("chatterbot.corpus.spanish.greetings")