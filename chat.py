from chatterbot import ChatBot
from chatterbot.conversation import Statement
from chatterbot.trainers import ListTrainer, ChatterBotCorpusTrainer

chat = ChatBot(                                               # se declara la entidad del chat, nueva instancia del bot
    'chatterbot',                                             # nombre del bot
    storage_adapter='chatterbot.storage.SQLStorageAdapter',   # adaptador para el almacenamiento en SQLite
    #database='./data/database.sqlite',                       # ubicacion del archivo SQLite donde se guardan los datos
    trainer='chatterbot.trainers.ListTrainer',                # adaptador logico para el entrenamiento
    #logic_adapters=[{                                        # clase que devuelve respuesta
        #"import_path": "chatterbot.logic.BestMatch",
        #"statement_comparison_function": "chatterbot.comparisons.levenshtein_distance",
        #"response_selection_method": "chatterbot.response_selection.get_most_frequent_response"
    #}]
)

trainer = ListTrainer(chat)
initialtrainer = ChatterBotCorpusTrainer(chat)
initialtrainer.train("./responses.yml")


def get_feedback():
    text = input()
    if 'si' in text.lower():
        return True
    elif 'no' in text.lower():
        return False
    else:
        print('Por favor escriba "Si" or "No"')
        return get_feedback()

# Lista donde se guardan las preguntas y respuestas para entrenar
last_input_statement = []

print('Escribe algo para empezar: ')

# The following loop will execute each time the user enters input
while True:
    try:
        # Se carga la pregunta del usuario
        input_statement = Statement(text=input())
        # Busca la respuesta
        response = chat.generate_response(input_statement)
        # Solicitar feedback
        print('\n Es esto "{}" una respuesta coherente para "{}"? \n'.format(
            response.text,
            input_statement.text
        ))
        response = response.text
        # Si la respuesta es incorrecta
        if get_feedback() is False:
            print('Por favor escriba la respuesta correcta: ')
            # Cargar respuesta brindada por el usuario
            response = input()
            # Cargar en la lista la respuesta
            # last_input_statement.append(input_statement.text)
            # last_input_statement.append(response)
            # Entranamiento del modelo con los nuevos valores
            # trainer.train(last_input_statement)
            print("Respuesta agregada al Chatbot")
            # Limpia la lista
            # last_input_statement.clear()


        # Cargar en la lista la respuesta
        last_input_statement.append(input_statement.text)
        last_input_statement.append(response)
        # Entranamiento del modelo con los nuevos valores
        # print(last_input_statement)
        trainer.train(last_input_statement)
        # Limpia la lista
        last_input_statement.clear()

    # Press ctrl-c or ctrl-d on the keyboard to exit
    except (KeyboardInterrupt, EOFError, SystemExit):
        break
